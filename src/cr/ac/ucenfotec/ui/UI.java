package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.CL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UI {
    private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private static CL gestor = new CL(); // una relación de asociación

    public static void main(String[] args) throws IOException {
        mostrarMenu();
    }

    public static void mostrarMenu() throws IOException{
        int opcion =0;
        do{
            System.out.println("**** Sistema Universidad ***");
            System.out.println("1. Registrar Carrera.");
            System.out.println("2. Listar Carreras.");
            System.out.println("3. Registrar Curso.");
            System.out.println("4. Listar Cursos.");
            System.out.println("5. Asociar un Curso a una Carrera.");
            System.out.println("6. Registrar grupo.");
            System.out.println("0. Salir");
            System.out.print("POr favor digite una opción: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while(opcion !=0);
    }

    public static void procesarOpcion(int opcion) throws IOException{
        switch (opcion){
            case 1: registrarCarrera();
                    break;
            case 2: listarCarreras();
                    break;
            case 3: registrarCurso();
                    break;
            case 4: listarCursos();
                    break;
            case 5: asociarCursoACarrera();
                break;
            case 6: registrarGrupo();
                break;
            case 0:
                System.out.println("Gracias por su visita");
            default:
                System.out.println("Opción invalida!");
        }
    }

    public static void registrarCarrera() throws IOException{
        System.out.print("Digite el código de la carrera: ");
        String codigo = in.readLine();
        System.out.print("Digite el nombre de la carrera: ");
        String nombre = in.readLine();
        System.out.print("Digite si es acreditada (1-Sí, 0-No): ");
        boolean esAcreditada = in.readLine().equals("1")?true:false;

        String respuesta = gestor.agregarCarrera(codigo,nombre,esAcreditada);
        System.out.println(respuesta);
    }

    public static void listarCarreras(){
        System.out.println("*** Listado de carreras inscritas ***");
        for (String carreraTemp :gestor.listarCarreras()) {
            System.out.println(carreraTemp);
        }
    }

    public static void registrarCurso() throws IOException{
        System.out.print("Digite el código del curso: ");
        String codigo = in.readLine();
        System.out.print("Digite el nombre del curso: ");
        String nombre = in.readLine();
        System.out.print("Digite la cantidad de créditos: ");
        int creditos = Integer.parseInt(in.readLine());
        System.out.print("Digite el costo del curso: ");
        double costo = Double.parseDouble(in.readLine());

        String respuesta = gestor.agregarCurso(codigo,nombre,creditos,costo);
        System.out.println(respuesta);
    }

    public static void listarCursos(){
        System.out.println("*** Listado de cursos registrados ***");
        for (String cursoTemp :gestor.listarCursos()) {
            System.out.println(cursoTemp);
        }
    }

    public static void asociarCursoACarrera() throws IOException{
        System.out.print("Digite el código de la carrera: ");
        String codigoCarrera = in.readLine();
        System.out.print("Digite el código del curso: ");
        String codigoCurso = in.readLine();

        String respuesta = gestor.asociarCursoACarrera(codigoCarrera,codigoCurso);
        System.out.println(respuesta);
    }

    public static void registrarGrupo() throws IOException{
        System.out.print("Digite el código del curso: ");
        String codigoCurso = in.readLine();
        System.out.print("Digite el número de grupo:  ");
        int numeroGrupo = Integer.parseInt(in.readLine());
        System.out.print("Digite el código del grupo: ");
        String codigoGrupo = in.readLine();

        String respuesta = gestor.registrarGrupo(codigoCurso,numeroGrupo,codigoGrupo);
        System.out.println(respuesta);
    }
}
